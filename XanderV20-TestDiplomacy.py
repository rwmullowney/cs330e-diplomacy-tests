#!/usr/bin/env python3

# -------
# imports
# -------


from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO('A Madrid Hold\nC Lisbon Move Madrid\nP Boston Hold\nZ Austin Support P')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nC [dead]\nP Boston\nZ Austin\n'
            )

    def test_solve_2(self):
        r = StringIO('A London Hold\nB Dublin Support C\nC Edinburgh Move London\nD Paris Support B')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB Dublin\nC London\nD Paris\n'
            )

    def test_solve_3(self):
        # free for all
        r = StringIO('A Ottawa Move WashingtonDC\nB WashingtonDC Hold\nC MexicoCity Move WashingtonDC')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\n'
            )

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
"""
